import java.util.List;

public class Flock {

    private final List<Bird> birds;
    private final  BirdFactory birdFactory;

    public Flock(List<String> types, BirdFactory birdFactory) {
        this.birdFactory = birdFactory;
        birds = types
                .stream().map(type -> birdFactory.createBird(type))
                .toList();
    }

    public List<String> getBirdsPlumage() {
        return birds
                .stream()
                .map(Bird::getPlumage)
                .toList();
    }

}
