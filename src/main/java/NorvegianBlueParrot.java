public class NorvegianBlueParrot extends Bird {
    public NorvegianBlueParrot(String type, int numberOfCoconut, int voltage) {
        super(type, numberOfCoconut, voltage);
    }

    @Override
    public String getPlumage() {
        return voltage() > 100 ? "scorched" : "beautiful";
    }
}
