import java.util.Objects;

public abstract class Bird {
    private final String type;
    private final int numberOfCoconut;
    private final int voltage;

    public Bird(String type, int numberOfCoconut, int voltage) {
        this.type = type;
        this.numberOfCoconut = numberOfCoconut;
        this.voltage = voltage;
    }

    public abstract String getPlumage();

    public String type() {
        return type;
    }

    public int numberOfCoconut() {
        return numberOfCoconut;
    }

    public int voltage() {
        return voltage;
    }



}