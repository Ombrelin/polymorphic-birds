public class AfricanSwallow extends Bird {
    public AfricanSwallow(String type, int numberOfCoconut, int voltage) {
        super(type, numberOfCoconut, voltage);
    }

    @Override
    public String getPlumage() {
        return numberOfCoconut() > 1 ? "tired" : "average";
    }
}
