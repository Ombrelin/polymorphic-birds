public class EuropeanSwallow extends Bird {
    public EuropeanSwallow(String type, int numberOfCoconut, int voltage) {
        super(type, numberOfCoconut, voltage);
    }

    @Override
    public String getPlumage() {
        return "average";
    }
}
