public class BirdFactory {
    public Bird createBird(String type) {
        return switch (type) {
            case "EuropeanSwallow" -> new EuropeanSwallow(type, 3, 4);
            case "AfricanSwallow" -> new AfricanSwallow(type, 5, 6);
            case "NorvegianBlueParrot" -> new NorvegianBlueParrot(type, 7, 8);
            default -> throw new IllegalStateException("Unexpected value: " + type);
        };
    }
}